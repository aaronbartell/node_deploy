# Summary
A simple example of how to deploy an application.

# Install
```
git clone
npm install
```

# Run 
## Development
```
npm start
```
## Production
This is how to start/stop the server when it is done outside of a deployment.
```
cd /www/myapp
./node_modules/pm2/bin/pm2 start server.js
./node_modules/pm2/bin/pm2 stop all
```

# Deploy
## First time
The first time you deploy you need to have a way to get the initial source down to the machine.
```
git clone ...
./deploy
```

## Subsequent time
```
./deploy
```
